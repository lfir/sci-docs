****
HTTP
****

Method role in RESTful APIs.
============================
* POST (sometimes PUT) - Create a new resource using the information sent with the request.
* GET - Read an existing resource without modifying it.
* PUT or PATCH (sometimes POST) - Update a resource using the data sent.
* DELETE - Delete a resource.

Sending arguments.
==================
GET method.
-----------
Sent as a query string after ? in the url. ::

    http://example.com/page?parameter=value&also=another

POST/PUT methods.
-----------------
Stored as key-value pairs in the body (not visible in the url). ::

    home=Cosby&favorite+flavor=flies
    { "key": "value" }


