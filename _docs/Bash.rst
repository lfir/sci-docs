****
Bash
****
.. highlight:: bash

Functions.
==========
Access arguments from within a function.
----------------------------------------
::

    All: "@"
    All starting after x: "${@:x}"
    Positional: $1, $2, $x
    Make argument list start after the first one: call shift or shift n to begin after the nth.

Define function.
----------------
::

    function hello () {
        echo "Hello world"
    }

Jobs.
=====
Pause current job. ::

    Ctrl+z

Restart last job. ::

    fg

Merge files in dir with cat and glob.
=====================================
Merges all files in the current dir into a single file. ::

    cat * >> bigfile.txt
    # Use *.extension instead of * to merge only files with the given extension.

Run command at logout.
======================
Add command to **~/.bash_logout**.

Run executable file.
====================
Mark the file as executable. ::

    chmod +x file-name.run

Execute the file in the terminal. ::

    ./file-name.run

Set environment variables.
==========================
For all users.
--------------
Add a line to **/etc/profile**. ::

    export VARNAME=value

Or create a file with **.sh extension** containing it in **/etc/profile.d**.

For a given user.
-----------------
Add line to **~/.bash_profile** or **~/.bashrc**.

* These statements are executed after user log in.

Use parts of last command in current one.
=========================================
::

    $ echo a
    a
    $ echo !:1
    a

    $ echo a b
    a b
    $ echo !:2
    b

    $ echo c d
    c d
    $ echo !:2 q !:1
    d q c

    $ rm somefile
    $ echo !:0
    rm

