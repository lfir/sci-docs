**
VI
**
.. highlight:: bash

Copy & paste.
=============
Command ‘Y’ or ‘yy’ copies (yanks) one or more lines. To copy one line. ::

        yw # Copy from cursor to the end of word.
        y$ # Copy from cursor to the end of the line.
        Y # Copy one line.
        2Y # Copy two lines.
	10Y # Copy ten lines.
	yG # Copy all lines to the end of the file.

Paste the text contained in the buffer. ::

	P # Paste above cursor.
        p # Paste below cursor.

Install plugins listed in ~/.vim/vimrc with Vundle.
===================================================
::

	:PluginInstall

List loaded plugins.
====================
::

	:scriptnames

Open nerdtree.
==============
::

	:NERDTree


