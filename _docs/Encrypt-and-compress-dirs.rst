********************************
Encrypt and compress directories
********************************
.. highlight:: bash

Using 7zip.
========================================
Backup.
-------
::

	tar cf - directory | 7za a -si -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on \
	-mhe=on -pmy_password bk-"$(date +%d-%m-%y)".tar.7z

List contents.
--------------
::

	7za x -so directory.tar.7z | tar tf - | less

Restore.
--------
::

	7za x -so directory.tar.7z | tar xf -

Verify.
-------
::

	7za t directory.tar.7z

Using gpg-zip.
========================================
Save dir.
---------
::

    gpg-zip --encrypt --output bk-"$(date +%d-%m-%y)".tar.gpg -r signingKeyEmail directory

List file contents.
-------------------
::

	gpg-zip --list-archive file.tar.gpg | less

Restore (using key in keyring).
-------------------------------
::

	gpg-zip --decrypt file.tar.gpg

