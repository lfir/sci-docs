*******
Postfix
*******
.. highlight:: bash

Allow host to relay through postfix.
====================================
Add host to **network_trust** file.

General valid syntax check.
===========================
::

    postfix check

Log subject, recipient and sender of emails.
=============================================
Add to **header_checks** file. ::

    /^subject:/      INFO
    /^to:/           INFO
    /^from:/         INFO

Test regexp matches.
====================
::

    postmap -q "Subject: news" regexp:/etc/postfix/rewrite_headers

Test transport file matches of addresses to transports.
=======================================================
::

    postmap -q "addr@dom.com" transport

.. highlight:: none

Rewrite subject of message based on recipient.
==============================================
In **transport** file add. ::

    destination@address.com custom_transport:

In **main.cf** add. ::

    header_checks = regexp:/etc/postfix/header_checks
    transport_maps = hash:/etc/postfix/transport

In **master.cf** add. ::

    # custom_transport is a copy of the smtp...smtp transport
    custom_transport ... smtp
      -o smtp_header_checks=regexp:/etc/postfix/rewrite_headers

In **rewrite_headers** add. ::

    /^Subject:(.*) / REPLACE Subject: fax from 555-1212

Finally reload postfix.

* `Reference. <https://serverfault.com/questions/866121/postfix-change-subject-based-on-recipient/875255#875255>`_

