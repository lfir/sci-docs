********
ZeroTier
********
.. highlight:: bash

Disconnect from network and delete its interface from the system.
=================================================================
::

    sudo zerotier-cli leave networkId

Join network.
=============
::

    sudo zerotier-cli join networkId

List joined networks status and assigned ips.
=============================================
::

    sudo zerotier-cli listnetworks

Show device status and id.
==========================
::

    sudo zerotier-cli info

Start service.
==============
::

    sudo systemctl start zerotier-one


* Commands tested using zerotier-one-1.2.12-1 on Fedora 29.
