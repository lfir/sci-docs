******
Ubuntu
******
.. highlight:: bash

Frequently used apt commands.
=============================
Install and remove packages.
----------------------------
Attempt to fix broken packages. ::

    sudo apt -f install # or remove.

Install package. ::

    sudo apt install pkgName

Search for package. ::

    apt search pkgName # and/or keywords.

Uninstall package. ::

    sudo apt remove pkgName

Uninstall pkg including config. files. ::

    sudo apt purge pkgName

Update the OS.
--------------
Install every update available (incl. kernels). ::

    sudo apt dist-upgrade

Install newer versions of the packages that don't replace or erase previously
installed ones. ::

    sudo apt upgrade

Update list of available packages and their versions. ::

    sudo apt update

.. highlight:: none

Upgrade the operating system to the latest release. ::

    sudo do-release-upgrade

Remote graphical session with x11vnc.
=====================================
Connect to the server using SSH.

Change to graphical runlevel if needed. ::

    sudo systemctl isolate graphical.target

Use one of the following options to forward the x11 session via vnc.

Before user login. ::

    sudo x11vnc -display :0 -auth /var/run/lightdm/root/:0

After login from the **home** dir. ::

    x11vnc -display :0 -auth .Xauthority

Access the server with a vnc client at serverIP:0.

* Tested on 16.04 (with display manager **lightdm**).

Turn on ufw and deny all incoming traffic.
==========================================
::

    sudo ufw enable && sudo ufw default deny


