.. Sci & Tech docs documentation master file, created by
   sphinx-quickstart on Sun Nov 18 14:45:53 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

***************************
Welcome to Sci & Tech docs!
***************************
This site contains excerpts from useful technical documentation I found online or wrote myself, and
uses syntax highlighting and cross document search to deliver a more efficient experience. It can
also be used offline by downloading and extracting this
`zipfile <https://gitlab.com/asta86/sci-docs/-/jobs/artifacts/master/download?job=pages>`_
and opening the file *index.html* in a browser.

Found an **error** or a **better way** to achieve something? Pull requests can be sent through the
*Edit on GitLab* button at the top of each document.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   _docs/Apache.rst
   _docs/Bacula.rst
   _docs/Bash.rst
   _docs/CSS-HTML.rst
   _docs/Dd.rst
   _docs/Docker.rst
   _docs/Docker-compose.rst
   _docs/Encrypt-and-compress-dirs.rst
   _docs/Fedora.rst
   _docs/Git.rst
   _docs/Grub.rst
   _docs/HTTP.rst
   _docs/Js.rst
   _docs/Logrotate.rst
   _docs/LVM.rst
   _docs/Make.rst
   _docs/Nagios.rst
   _docs/Network.rst
   _docs/Postfix.rst
   _docs/PostgreSQL.rst
   _docs/Python3.rst
   _docs/Sched.rst
   _docs/Textproc.rst
   _docs/Tmux.rst
   _docs/Ubuntu.rst
   _docs/VI.rst
   _docs/Zerotier.rst

.. toctree::
   :maxdepth: 1
   :caption: About

   _docs/Contact.rst

